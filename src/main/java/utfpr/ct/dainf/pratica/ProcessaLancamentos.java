package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Linguagem Java
 *
 * @author
 */
public class ProcessaLancamentos {

    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        try {
            reader = new BufferedReader(new FileReader(arquivo));
        } catch (FileNotFoundException fnfe) {
            System.out.println("Arquivo Não encontrado: " + fnfe.getLocalizedMessage());
        }
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {

    }

    private String getNextLine() throws IOException {
        return reader.readLine();
    }

    private Lancamento processaLinha(String linha) throws ParseException {

        Integer conta = Integer.parseInt(linha.substring(0, 5));
        Date data = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(linha.substring(7, 13));
        String descricao = linha.substring(15, 73);
        Double valor = Double.parseDouble(linha.substring(0, 5));

        Lancamento novo = new Lancamento(conta, data, descricao, valor);

        return novo;
    }

    private Lancamento getNextLancamento() throws IOException, ParseException {

        Lancamento novo = this.processaLinha(this.getNextLine());
        return novo;
    }

    public List<Lancamento> getLancamentos() throws IOException, ParseException {
        ArrayList<Lancamento> lancamentos = new ArrayList<>();
        Lancamento novo;
        while ((novo = this.getNextLancamento()) != null) {
            lancamentos.add(novo);
        }

        LancamentoComparator comparador = new LancamentoComparator();

        lancamentos.sort(comparador);

        return lancamentos;
    }

}
